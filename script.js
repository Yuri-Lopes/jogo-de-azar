function getRandom(max) {
    return Math.floor(Math.random() * max + 1)
}

function mensagem(p) {
    const saida = 'h';
    var mensagens = document.getElementById("mensagem");
    switch (p) {

        case 1:
            mensagens.textContent = "Algumas vezes um homem inteligente é forçado a ficar bêbado para passar um tempo com os burros.";
            break;
        case 2:
            mensagens.textContent = "Ser são é fácil, mas pra ser bêbado tem que ter talento.";
            break;

        case 3:
            mensagens.textContent = "Dizer que um crente é mais feliz do que um cético é como dizer que um bêbado é mais feliz do que um sóbrio.";
            break;

        case 4:
            mensagens.textContent = "Faça sempre lúcido o que você disse que faria bêbado. Isso o ensinará a manter sua boca fechada.";
            break;

        case 5:
            mensagens.textContent = "O melhor é sair da vida como de uma festa, nem sedento nem bêbado.";
            break;

        case 6:
            mensagens.textContent = "Nunca devemos lamentar que um poeta seja um bêbado, devemos lamentar que nem todos os bêbados sejam poetas.";
            break;

        case 7:
            mensagens.textContent = "A realidade é um bêbado jogado no chão, até que alguém bata na minha porta e prove o contrário.";
            break;

        case 8:
            mensagens.textContent = "Eu não deveria dirigir! Espera ai, eu não deveria me ouvir, estou bêbado.";
            break;
    }
}
